import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;
import static org.junit.Assert.assertTrue;

public class WhiskUITest {

    @Before
    public void setUp() {
        Configuration.startMaximized = true;
        Configuration.timeout = 15000;
    }

    @Test
    public void addingItemsToShoppingListTest() {
        open("https://my.whisk-dev.com/");
        signIn("email1@email.com", "test23@#");
        clickOnMenuItem("Shopping");
        createShoppingListWithName("ShoppingList");
        addItemsToShoppingList("Milk", "Bread", "Onion", "Butter", "Cheese");
        checkByNameThatItemsAreAddedToShoppingList("Milk", "Bread", "Onion", "Butter", "Cheese");
        closeWebDriver();
    }

    @Test
    public void deleteShoppingListTest() {
        open("https://my.whisk-dev.com/");
        signIn("email1@email.com", "test23@#");
        clickOnMenuItem("Shopping");
        createShoppingListWithName("ShoppingList");
        deleteShoppingList("ShoppingList");
        checkThatPageDoesNotHaveText("ShoppingList");
        closeWebDriver();
    }

    private void checkThatPageDoesNotHaveText(String text) {
        $(byTagName("body")).shouldNotHave(text(text));
    }

    private void deleteShoppingList(String listName) {
        while (0 < newShoppingList(listName).size()) {
            editButton(listName).click();
            $(byText("Delete list")).shouldBe(visible).click();
            $(byText("Confirm delete")).shouldBe(visible).click();
            $(byText("List was deleted")).shouldHave(disappear);
        }
    }

    private static ElementsCollection newShoppingList(String listName) {
        return  $$(byXpath("//*[contains(text(), '"+listName+"')]"));
    }

    private static SelenideElement editButton(String listName) {
        return  $(byXpath("//*[contains(text(), '"+listName+"')]/parent::div/following-sibling::div//button"));
    }

    private void checkByNameThatItemsAreAddedToShoppingList(String... productName) {
        String errorMessage1 = "Product Milk not added to list";
        String errorMessage2 = "Product Bread not added to list";
        String errorMessage3 = "Product Onion not added to list";
        String errorMessage4 = "Product Butter not added to list";
        String errorMessage5 = "Product Cheese not added to list";

        assertTrue(errorMessage1, listOfProducts().texts().contains(productName[0]));
        assertTrue(errorMessage2, listOfProducts().texts().contains(productName[1]));
        assertTrue(errorMessage3, listOfProducts().texts().contains(productName[2]));
        assertTrue(errorMessage4, listOfProducts().texts().contains(productName[3]));
        assertTrue(errorMessage5, listOfProducts().texts().contains(productName[4]));
    }

    private ElementsCollection listOfProducts() {
        return $$(By.cssSelector("span[data-testid='shopping-list-item-name']"));
    }

    private void addItemsToShoppingList(String... productName) {
        $(By.cssSelector("input[placeholder='Add item']")).shouldBe(visible).click();
        $(byText(productName[0])).shouldBe(visible).click();
        $(byText(productName[1])).shouldBe(visible).click();
        $(byText(productName[2])).shouldBe(visible).click();
        $(byText(productName[3])).shouldBe(visible).click();
        $(byText(productName[4])).shouldBe(visible).click();
        $(byText("ShoppingList")).shouldBe(visible).click();
    }

    private void createShoppingListWithName(String listName) {
        $(byText("Create new list")).shouldBe(visible).click();
        $(By.name("name")).sendKeys(Keys.CONTROL + "A");
        $(By.name("name")).sendKeys(Keys.BACK_SPACE);
        $(By.name("name")).shouldBe(visible).val(listName);
        $(byText("Create")).shouldBe(visible).click();
        $(byText("ShoppingList")).shouldBe(visible);
    }

    private void clickOnMenuItem(String menuName) {
        $(byText(menuName)).shouldBe(visible).click();
    }

    private void signIn(String email, String password) {
        $(By.name("username")).shouldBe(visible).val(email);
        $(byText("Continue")).shouldBe(visible).click();
        $(By.name("password")).shouldBe(visible).val(password);
        $(By.cssSelector("button[type='submit']")).shouldBe(visible).click();
    }
}
