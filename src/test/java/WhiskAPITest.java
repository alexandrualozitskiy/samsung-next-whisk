import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import static io.restassured.RestAssured.given;

public class WhiskAPITest {
    private int statusCode;
    private String createdListID;
    private String listID;
    private String content;
    private String code;

    @Before
    public void createShoppingList() {
        Response response = getClient()
                .post("/list/v2")
                .then()
                .extract().response();

        createdListID = response.path("list.id");
    }

    @Test
    @Order(1)
    public void createShoppingListAPITest() {
        getShoppingListByID(createdListID);

        Assert.assertEquals("The response does not contain the necessary id", createdListID, listID);
        Assert.assertNull("Shopping list is not empty", content);
    }

    @Test
    @Order(2)
    public void deleteShoppingListAPITest() {
        deleteShoppingListByID(createdListID);
        getShoppingListByID(createdListID);

        Assert.assertEquals("The status code is not 400" , 400 , statusCode);
        Assert.assertEquals("The response message is not 'shoppingList.notFound'",
                "shoppingList.notFound", code);
    }

    private void deleteShoppingListByID(String ID) {
        getClient().delete("/list/v2/" + ID);
    }

    private void getShoppingListByID(String ID) {
        Response getShoppingListById = getClient()
                .get("/list/v2/" + ID)
                .then()
                .extract().response();

        listID = getShoppingListById.path("list.id");
        content = getShoppingListById.path("list.content");
        statusCode = getShoppingListById.getStatusCode();
        code = getShoppingListById.path("code");
    }

    public RequestSpecification getClient() {
        String BASE_URI = "https://api.whisk-dev.com";
        String token = "9IgHFLUZX1aKu7Q0OAYgq4rhh4YEg8ZzZq5cIU336qBWq3OwPRnmjRzyhCY2EooW";
        String tokenType = "Bearer";

        return given()
                .baseUri(BASE_URI)
                .header(new Header("Authorization", tokenType + " " + token));
    }
}
